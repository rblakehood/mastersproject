package com.zarokima.mastersproject.game;

import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import android.content.Context;
import android.graphics.PointF;
import android.util.Log;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.zarokima.mastersproject.util.ICollidable;
import com.zarokima.mastersproject.util.PhysicsWorldHelper;
import com.zarokima.mastersproject.util.SceneHelper;
import com.zarokima.mastersproject.util.UserData;

//is shot by gun
public class Bullet implements ICollidable
{
	private static String spritePath = "Bullet.png"; 
	
	protected Sprite sprite;
	protected Body body;
	protected BitmapTextureAtlas textureAtlas;
	protected TiledTextureRegion textureRegion;
	protected FixtureDef fixtureDef = PhysicsFactory.createFixtureDef(0, 0f, 0.7f);
	protected UserData userData;
	protected GameActivity context;
	
	private boolean markedForDeletion = false;
	
	public Bullet(Context c, PointF p, TextureManager textureManager, VertexBufferObjectManager vbom, Vector2 velocity)
	{
		context = (GameActivity) c;
		PhysicsWorld physicsWorld = PhysicsWorldHelper.GetPhysicsWorld();
		textureAtlas = new BitmapTextureAtlas(textureManager, 16, 16, TextureOptions.BILINEAR);
		textureAtlas.load();
		textureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(textureAtlas, c, spritePath, 0, 0, 1, 1);
		sprite = new Sprite(p.x - textureRegion.getWidth()/2, p.y - textureRegion.getHeight()/2, textureRegion, vbom);
		body = PhysicsFactory.createCircleBody(physicsWorld, sprite, BodyType.DynamicBody, fixtureDef);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite, body, true, true));
		sprite.setUserData(body);

		userData = new UserData();
		userData.killOnContact = true;
		userData.container = this;
		body.setUserData(userData);
		SceneHelper.GetScene().attachChild(sprite);
		body.setLinearVelocity(velocity);
	}

	//destroy when it hits anything
	@Override
	public void beginCollideWith(Body other)
	{
		if(markedForDeletion)
		{
			return;
		}
		markedForDeletion = true;
		final Bullet bullet = this;
		context.runOnUpdateThread(new Runnable()
		{
            @Override
            public void run() 
            {
            	bullet.sprite.detachSelf();
            	bullet.sprite.clearUpdateHandlers();
                Body body = bullet.body;
                PhysicsWorld physicsWorld = PhysicsWorldHelper.GetPhysicsWorld(); 
                physicsWorld.unregisterPhysicsConnector(physicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(bullet.sprite));
                physicsWorld.destroyBody(body);
            }
        });
	}

	@Override
	public void endCollideWith(Body other)
	{
		// TODO Auto-generated method stub
		
	}
}
