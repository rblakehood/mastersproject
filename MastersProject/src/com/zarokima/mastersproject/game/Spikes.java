package com.zarokima.mastersproject.game;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class Spikes extends Wall
{

	public Spikes(int x, int y, int width, int height, VertexBufferObjectManager vbom)
	{
		super(x, y, width, height, vbom);
		userData.container = this;
		userData.killOnContact = true;
		body.setUserData(userData);
	}

}
