package com.zarokima.mastersproject.game;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import com.badlogic.gdx.math.Vector2;
import android.content.Context;
import android.graphics.PointF;
import android.util.Log;

//shoots bullets
public class Gun extends Environment
{
	private static String spritePath = "Gun.png";
	private float shootDelay = 1;
	private int shootSpeed = 5; 
	
	public Gun(final Context c, PointF p, final TextureManager textureManager, final VertexBufferObjectManager vbom, int direction, int delay, int speed)
	{
		super(c, p, textureManager, spritePath, vbom);
		shootDelay = delay;
		shootSpeed = speed;
		
		//confused about these "magic" numbers?
		//look at the number pad on your keyboard
		switch(direction)
		{
			case 2:
				body.setTransform(body.getPosition(), (float) Math.toRadians(180));
				break;
			case 4:
				body.setTransform(body.getPosition(), (float) Math.toRadians(270));
				break;
			case 6:
				body.setTransform(body.getPosition(), (float) Math.toRadians(90));
				break;
			//nothing needed for 8 (up)
		}
		
		userData.container = this;
		body.setUserData(userData);
		
		GameActivity activity = (GameActivity) c;
		activity.getEngine().registerUpdateHandler(new TimerHandler(shootDelay, true, new ITimerCallback()
		{

			@Override
			public void onTimePassed(TimerHandler pTimerHandler)
			{
				ShootBullet(c, textureManager, vbom);
			}
			
		}));
	}
	
	public void ShootBullet(Context c, TextureManager textureManager, VertexBufferObjectManager vbom)
	{
		Vector2 velocity = new Vector2(0, -shootSpeed);
		velocity = velocity.rotate(sprite.getRotation());
		float[] coords = sprite.convertLocalToSceneCoordinates(16, -16);
		PointF location = new PointF(coords[0], coords[1]);
		new Bullet(c, location, textureManager, vbom, velocity);
	}
}
