package com.zarokima.mastersproject.game;

import java.util.ArrayList;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import android.content.Context;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.Log;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.zarokima.mastersproject.util.GravityHelper;
import com.zarokima.mastersproject.util.ICollidable;
import com.zarokima.mastersproject.util.PhysicsWorldHelper;
import com.zarokima.mastersproject.util.SceneHelper;
import com.zarokima.mastersproject.util.UserData;

public class Character implements ICollidable
{
	protected AnimatedSprite sprite;
	protected Body body;
	protected BitmapTextureAtlas textureAtlas;
	protected TiledTextureRegion textureRegion;
	protected FixtureDef fixtureDef = PhysicsFactory.createFixtureDef(1, 0f, 0.7f);
	protected float moveSpeed = 5;
	protected float jumpForce = -7.5f;
	protected IUpdateHandler movementHandler;
	protected boolean grounded;
	protected UserData userData;
	protected ArrayList<Body> touchingGrounds;
	protected GameActivity context;

	// ==============
	// |Constructors|
	// ==============
	public Character(Context c, PointF p, TextureManager textureManager, String path,
			VertexBufferObjectManager vbom)
	{
		PhysicsWorld physicsWorld = PhysicsWorldHelper.GetPhysicsWorld();
		context = (GameActivity) c;
		touchingGrounds = new ArrayList<Body>();
		textureAtlas = new BitmapTextureAtlas(textureManager, 64, 64, TextureOptions.BILINEAR);
		textureAtlas.load();
		textureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(textureAtlas, c, path, 0, 0,
				2, 1);
		sprite = new AnimatedSprite(p.x, p.y, textureRegion, vbom);
		sprite.animate(new long[] { 200, 200 }, 0, 1, true);
		body = PhysicsFactory.createBoxBody(physicsWorld, sprite, BodyType.DynamicBody, fixtureDef);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite, body, true, true));
		sprite.setUserData(body);

		if (userData == null)
			userData = new UserData();

		userData.isCharacter = true;
		userData.container = this;
		body.setUserData(userData);
		SceneHelper.GetScene().attachChild(sprite);
		
		grounded = true;
	}

	// =========
	// |Getters|
	// =========
	public AnimatedSprite getSprite()
	{
		return sprite;
	}

	public Body getBody()
	{
		return body;
	}

	public PointF getPosition()
	{
		return new PointF(sprite.getX(), sprite.getY());
	}

	public BitmapTextureAtlas getTextureAtlas()
	{
		return textureAtlas;
	}

	public TiledTextureRegion getTextureRegion()
	{
		return textureRegion;
	}

	// =========
	// |Setters|
	// =========
	public void setTextureAtlas(BitmapTextureAtlas textureAtlas)
	{
		this.textureAtlas = textureAtlas;
	}

	public void setTiledTextureRegion(TiledTextureRegion textureRegion)
	{
		this.textureRegion = textureRegion;
	}

	public void setPosition(float x, float y)
	{
		sprite.setPosition(x, y);
	}
	
	public void setPosition(Point p)
	{
		sprite.setPosition(p.x, p.y);
	}

	// =========
	// |Methods|
	// =========
	public void MoveLeft(Scene scene)
	{
		scene.unregisterUpdateHandler(movementHandler);
		movementHandler = new IUpdateHandler()
		{

			@Override
			public void onUpdate(float pSecondsElapsed)
			{
				Vector2 current = GravityHelper.CalculateUnRotatedVelocity(body.getLinearVelocity());
				Vector2 velocity = GravityHelper.CalculateDirectionalVelocity(new Vector2(-moveSpeed, current.y));
				body.setLinearVelocity(velocity);
			}

			@Override
			public void reset()
			{
				// TODO Auto-generated method stub

			}

		};

		scene.registerUpdateHandler(movementHandler);
	}

	public void MoveRight(Scene scene)
	{
		scene.unregisterUpdateHandler(movementHandler);
		movementHandler = new IUpdateHandler()
		{

			@Override
			public void onUpdate(float pSecondsElapsed)
			{
				Vector2 current = GravityHelper.CalculateUnRotatedVelocity(body.getLinearVelocity());
				Vector2 velocity = GravityHelper.CalculateDirectionalVelocity(new Vector2(moveSpeed, current.y));
				body.setLinearVelocity(velocity);
			}

			@Override
			public void reset()
			{
				// TODO Auto-generated method stub

			}

		};

		scene.registerUpdateHandler(movementHandler);
	}

	public void StopMovement(Scene scene)
	{
		scene.unregisterUpdateHandler(movementHandler);
		
		Vector2 current = GravityHelper.CalculateUnRotatedVelocity(body.getLinearVelocity());
		Vector2 velocity = GravityHelper.CalculateDirectionalVelocity(new Vector2(0, current.y));
		body.setLinearVelocity(velocity);
	}

	public void Jump()
	{
		if(!grounded)
			return;
		
		Vector2 current = GravityHelper.CalculateUnRotatedVelocity(body.getLinearVelocity());
		Vector2 velocity = GravityHelper.CalculateDirectionalVelocity(new Vector2(current.x, jumpForce));
		body.setLinearVelocity(velocity);
	}

	public void UpdateRotation()
	{
		if (!grounded)
		{
			body.setTransform(body.getPosition(), (float) Math.toRadians(180 - GravityHelper.GetGravityAngle()));
		}
	}

	public void die()
	{
		final Character character = this;
		context.runOnUpdateThread(new Runnable()
		{
            @Override
            public void run() 
            {
            	character.sprite.detachSelf();
                Body body = character.body;
                PhysicsWorld physicsWorld = PhysicsWorldHelper.GetPhysicsWorld(); 
                physicsWorld.unregisterPhysicsConnector(physicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(character.getSprite()));
                physicsWorld.destroyBody(body);
            }
        });
		SceneHelper.GetScene().unregisterUpdateHandler(movementHandler);
	}
	
	@Override
	public void beginCollideWith(Body other)
	{
		UserData ud = (UserData) other.getUserData();
		Vector2 relativeLocation = body.getLocalPoint(other.getWorldCenter());
		
		if(ud.killOnContact)
		{
			die();
			return;
		}
		
		if (ud.isGround && relativeLocation.y > 0)
		{
			grounded = true;
			if(!touchingGrounds.contains(other))
			{
				touchingGrounds.add(other);
			}	
		}
	}

	@Override
	public void endCollideWith(Body other)
	{
		UserData ud = (UserData) other.getUserData();
		if (ud.isGround)
		{
			touchingGrounds.remove(other);		
			grounded = !touchingGrounds.isEmpty();
		}
	}
}