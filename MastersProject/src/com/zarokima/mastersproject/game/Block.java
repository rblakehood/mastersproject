package com.zarokima.mastersproject.game;

import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import android.content.Context;
import android.graphics.PointF;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.zarokima.mastersproject.util.ICollidable;
import com.zarokima.mastersproject.util.PhysicsWorldHelper;
import com.zarokima.mastersproject.util.SceneHelper;
import com.zarokima.mastersproject.util.UserData;

//It's basically a wall that's free to move around and affected by gravity
public class Block implements ICollidable
{
	protected Sprite sprite;
	protected Body body;
	protected BitmapTextureAtlas textureAtlas;
	protected TiledTextureRegion textureRegion;
	protected FixtureDef fixtureDef = PhysicsFactory.createFixtureDef(1, 0f, 0f);
	protected UserData userData;
	private static String spritePath = "Block.png";
	
	public Block(Context c, PointF p, int width, int height, TextureManager textureManager, VertexBufferObjectManager vbom)
	{
		PhysicsWorld physicsWorld = PhysicsWorldHelper.GetPhysicsWorld();
		textureAtlas = new BitmapTextureAtlas(textureManager, 32, 32, TextureOptions.BILINEAR);
		textureAtlas.load();
		textureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(textureAtlas, c, spritePath, 0, 0, 1, 1);
		sprite = new Sprite(p.x, p.y, width, height, textureRegion, vbom);
		body = PhysicsFactory.createBoxBody(physicsWorld, sprite, BodyType.DynamicBody, fixtureDef);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite, body, true, true));
		sprite.setUserData(body);

		userData = new UserData();
		userData.isGround = true;
		userData.container = this;
		body.setUserData(userData);
		SceneHelper.GetScene().attachChild(sprite);
	}
	
	@Override
	public void beginCollideWith(Body other)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endCollideWith(Body other)
	{
		// TODO Auto-generated method stub
		
	}
}
