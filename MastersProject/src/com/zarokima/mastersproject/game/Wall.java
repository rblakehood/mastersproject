package com.zarokima.mastersproject.game;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.zarokima.mastersproject.util.ICollidable;
import com.zarokima.mastersproject.util.PhysicsWorldHelper;
import com.zarokima.mastersproject.util.SceneHelper;
import com.zarokima.mastersproject.util.UserData;

//an invisible wall -- will be "shown" via the tilemap
public class Wall implements ICollidable
{
	protected Rectangle rect;
	protected Body body;
	protected FixtureDef fixtureDef = PhysicsFactory.createFixtureDef(0, 0, 1);
	protected UserData userData;
	
	public Wall(int x, int y, int width, int height, VertexBufferObjectManager vbom)
	{
		PhysicsWorld physicsWorld = PhysicsWorldHelper.GetPhysicsWorld();
		
		rect = new Rectangle(x, y, width, height, vbom);
		rect.setVisible(false);
		body = PhysicsFactory.createBoxBody(physicsWorld, rect, BodyType.StaticBody, fixtureDef);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(rect, body));
		rect.setUserData(body);
		SceneHelper.GetScene().attachChild(rect);
		
		userData = new UserData();
		userData.isGround = true;
		userData.container = this;
		body.setUserData(userData);
	}

	@Override
	public void beginCollideWith(Body other)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endCollideWith(Body other)
	{
		// TODO Auto-generated method stub
		
	}
}
