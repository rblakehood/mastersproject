package com.zarokima.mastersproject.game;

import org.andengine.entity.scene.Scene;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import android.content.Context;
import android.graphics.PointF;
import com.badlogic.gdx.physics.box2d.Body;

public class Goal extends Environment
{
	private static String spritePath = "Goal.png";

	public Goal(Context c, PointF p, TextureManager textureManager,	VertexBufferObjectManager vbom)
	{
		super(c, p, textureManager, spritePath, vbom);
		userData.container = this;
		userData.isGoal = true;
		body.setUserData(userData);
	}

	@Override
	public void beginCollideWith(Body other)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endCollideWith(Body other)
	{
		// TODO Auto-generated method stub
		
	}
}
