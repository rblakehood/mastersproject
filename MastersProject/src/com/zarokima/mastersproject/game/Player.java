package com.zarokima.mastersproject.game;

import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import android.content.Context;
import android.graphics.PointF;
import com.badlogic.gdx.physics.box2d.Body;
import com.zarokima.mastersproject.util.UserData;

public class Player extends Character
{
	private static String spritePath = "face_box_tiled.png";
	
	public Player(Context c, PointF p,
			TextureManager textureManager, VertexBufferObjectManager vbom)
	{
		super(c, p, textureManager, spritePath, vbom);
	}
	
	@Override
	public void die()
	{
		super.die();
		context.playerDied();
	}
	
	@Override
	public void beginCollideWith(Body other)
	{
		UserData ud = (UserData) other.getUserData();
		
		if(ud.isGoal)
		{
			context.onLevelComplete();
			return;
		}
		
		super.beginCollideWith(other);
	}

	@Override
	public void endCollideWith(Body other)
	{
		super.endCollideWith(other);
	}
}
