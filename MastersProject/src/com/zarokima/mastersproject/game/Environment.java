package com.zarokima.mastersproject.game;

import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import android.content.Context;
import android.graphics.PointF;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.zarokima.mastersproject.util.ICollidable;
import com.zarokima.mastersproject.util.PhysicsWorldHelper;
import com.zarokima.mastersproject.util.SceneHelper;
import com.zarokima.mastersproject.util.UserData;

public class Environment implements ICollidable
{
	protected Sprite sprite;
	protected Body body;
	protected BitmapTextureAtlas textureAtlas;
	protected TiledTextureRegion textureRegion;
	protected FixtureDef fixtureDef = PhysicsFactory.createFixtureDef(0, 0f, 0.7f);
	protected UserData userData;

	// ==============
	// |Constructors|
	// ==============
	public Environment(Context c, PointF p, TextureManager textureManager, String path,
			VertexBufferObjectManager vbom)
	{
		PhysicsWorld physicsWorld = PhysicsWorldHelper.GetPhysicsWorld();
		textureAtlas = new BitmapTextureAtlas(textureManager, 32, 32, TextureOptions.BILINEAR);
		textureAtlas.load();
		textureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(textureAtlas, c, path, 0, 0, 1, 1);
		sprite = new Sprite(p.x, p.y, textureRegion, vbom);
		body = PhysicsFactory.createBoxBody(physicsWorld, sprite, BodyType.StaticBody, fixtureDef);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite, body, true, true));
		sprite.setUserData(body);

		userData = new UserData();
		userData.isGround = true;
		userData.container = this;
		body.setUserData(userData);
		SceneHelper.GetScene().attachChild(sprite);
	}

	// =========
	// |Getters|
	// =========
	public Sprite getSprite()
	{
		return sprite;
	}

	public Body getBody()
	{
		return body;
	}

	public PointF getPosition()
	{
		return new PointF(sprite.getX(), sprite.getY());
	}

	public BitmapTextureAtlas getTextureAtlas()
	{
		return textureAtlas;
	}

	public TiledTextureRegion getTextureRegion()
	{
		return textureRegion;
	}

	// =========
	// |Setters|
	// =========
	public void setTextureAtlas(BitmapTextureAtlas textureAtlas)
	{
		this.textureAtlas = textureAtlas;
	}

	public void setTiledTextureRegion(TiledTextureRegion textureRegion)
	{
		this.textureRegion = textureRegion;
	}

	public void setPosition(float x, float y)
	{
		sprite.setPosition(x, y);
	}

	@Override
	public void beginCollideWith(Body other)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void endCollideWith(Body other)
	{
		// TODO Auto-generated method stub

	}
}
