package com.zarokima.mastersproject.game;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.extension.physics.box2d.util.Vector2Pool;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.input.sensor.acceleration.AccelerationData;
import org.andengine.input.sensor.acceleration.IAccelerationListener;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import com.badlogic.gdx.math.Vector2;
import com.zarokima.mastersproject.MenuActivity;
import com.zarokima.mastersproject.util.GravityHelper;
import com.zarokima.mastersproject.util.MapLoader;
import com.zarokima.mastersproject.util.MastersContactListener;
import com.zarokima.mastersproject.util.PhysicsWorldHelper;
import com.zarokima.mastersproject.util.SceneHelper;

public class GameActivity extends SimpleBaseGameActivity implements IAccelerationListener, IOnSceneTouchListener
{
	// ===========================================================
	// Constants
	// ===========================================================

	private static final int JUMP_TOUCH_DISTANCE = 30;

	// ===========================================================
	// Camera
	// ===========================================================
	private static int CAMERA_WIDTH = 480;
	private static int CAMERA_HEIGHT = 320;

	// ===========================================================
	// Fields
	// ===========================================================
	private BoundCamera camera;

	private Character player;

	private String currentMap = "tmx/test.tmx";
	// ===========================================================
	// Constructors
	// ===========================================================

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		if (extras != null && extras.getString("level") != null)
		{
			currentMap = "tmx/" + extras.getString("level") + ".tmx";
		}
		else
		{
			Log.d("GameActivity", "Intent has no extras");
			this.finish();
		}
	}
	
	@Override
	public EngineOptions onCreateEngineOptions()
	{
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		camera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);

		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(metrics.widthPixels,
				metrics.heightPixels), camera);
	}

	@Override
	public void onCreateResources()
	{
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
	}

	@Override
	public Scene onCreateScene()
	{
		this.mEngine.registerUpdateHandler(new FPSLogger());

		SceneHelper.ResetScene();		
		Scene scene = SceneHelper.GetScene();
		scene.setBackground(new Background(0, 0, 0));
		scene.setOnSceneTouchListener(this);

		PhysicsWorldHelper.ResetPhysicsWorld();
		PhysicsWorld physicsWorld = PhysicsWorldHelper.GetPhysicsWorld();

		final VertexBufferObjectManager vertexBufferObjectManager = this.getVertexBufferObjectManager();
		final TextureManager textureManager = this.getTextureManager();

		MapLoader.LoadMap(this, camera, this.getAssets(), textureManager,
				TextureOptions.BILINEAR_PREMULTIPLYALPHA, vertexBufferObjectManager, currentMap);
		
		// set up the player
		player = new Player(this, MapLoader.playerSpawn, textureManager, vertexBufferObjectManager);
		camera.setChaseEntity(player.getSprite());
		scene.registerUpdateHandler(new IUpdateHandler()
		{

			@Override
			public void onUpdate(float pSecondsElapsed)
			{
				player.UpdateRotation();
			}

			@Override
			public void reset()
			{
				// TODO Auto-generated method stub

			}

		});
		
		scene.registerUpdateHandler(physicsWorld);

		return scene;
	}

	@Override
	public boolean onSceneTouchEvent(final Scene pScene, final TouchEvent event)
	{
		if ( player != null)
		{
			Scene scene = SceneHelper.GetScene();
			if (event.isActionDown())
			{
				float relativeCoords[] = player.getSprite().convertSceneToLocalCoordinates(event.getX(), event.getY());
				Vector2 pointRelativeToPlayer = new Vector2(relativeCoords[0], relativeCoords[1]);
				pointRelativeToPlayer.sub(player.getSprite().getWidth() / 2, player.getSprite().getHeight() / 2);
				if (Math.abs(pointRelativeToPlayer.x) < JUMP_TOUCH_DISTANCE)
				{
					player.Jump();
				}
				else if (pointRelativeToPlayer.x > 0)
				{
					player.MoveLeft(scene);
				}
				else
				{
					player.MoveRight(scene);
				}
				
			}
			else if (event.isActionUp())
			{
				player.StopMovement(scene);
			}
		}
		return false;
	}

	@Override
	public void onAccelerationAccuracyChanged(final AccelerationData pAccelerationData)
	{

	}

	@Override
	public void onAccelerationChanged(final AccelerationData pAccelerationData)
	{
		float gravityX = pAccelerationData.getX();
		float gravityY = pAccelerationData.getY();

		final Vector2 gravity = Vector2Pool.obtain(gravityX, gravityY);
		GravityHelper.currentGravity = gravity;
		PhysicsWorldHelper.GetPhysicsWorld().setGravity(gravity);
		Vector2Pool.recycle(gravity);
	}

	@Override
	public void onResumeGame()
	{
		super.onResumeGame();

		this.enableAccelerationSensor(this);
	}

	@Override
	public void onPauseGame()
	{
		super.onPauseGame();

		this.disableAccelerationSensor();
	}
	
	public void playerDied()
	{
		Intent intent = getIntent();
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		finish();
		startActivity(intent);
	}
	
	public void onLevelComplete()
	{
		ArrayList<String> completeLevels = new ArrayList<String>();
		try
		{
			BufferedReader buf;
			buf = new BufferedReader(new InputStreamReader(openFileInput(MenuActivity.SAVE_FILE)));
			while(buf.ready())
			{
				completeLevels.add(buf.readLine());
			}
		}
		catch(Exception e)
		{
			//save file does not exist yet, no big deal
		}
		
		try
		{
			if(!completeLevels.contains(getIntent().getExtras().getString("level")))
			{
				completeLevels.add(getIntent().getExtras().getString("level"));
			}
			
			OutputStreamWriter osw = new OutputStreamWriter(openFileOutput(MenuActivity.SAVE_FILE, Context.MODE_PRIVATE));
			String output = "";
			for(String s : completeLevels)
			{
				output += s + "\n";
			}
			osw.write(output);
			osw.close();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Intent returnIntent = new Intent();
		setResult(RESULT_OK, returnIntent);
		finish();
	}
}
