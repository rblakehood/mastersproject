package com.zarokima.mastersproject;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import com.zarokima.mastersproject.game.GameActivity;
import com.zarokima.mastersproject.util.LevelAdapter;

public class MenuActivity extends Activity implements OnClickListener
{
	public static final String SAVE_FILE = "MastersProjectSaveData";

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		setContentView(R.layout.levelselectmenu);
		GridView gridview = (GridView) findViewById(R.id.gridview);
		gridview.setAdapter(new LevelAdapter(this));
	}

	public void onClick(View view)
	{
		Button b = (Button) view;
		Intent newIntent = new Intent(this, GameActivity.class);
		newIntent.putExtra("level", b.getText());
		startActivityForResult(newIntent, 1);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.w("MENU", "onactivityresult");
		if (requestCode == 1 && resultCode == RESULT_OK)
		{
			Log.w("MENU", "inside if");
			//restart activity so level completion display updates
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		}
	}
}
