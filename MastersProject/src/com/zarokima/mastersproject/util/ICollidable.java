package com.zarokima.mastersproject.util;

import com.badlogic.gdx.physics.box2d.Body;

public interface ICollidable
{	
	public void beginCollideWith(Body other);
	public void endCollideWith(Body other);
}
