package com.zarokima.mastersproject.util;

import org.andengine.entity.scene.Scene;

//singleton so I don't have to pass it around everywhere from the game activity
public class SceneHelper
{
	private static Scene scene;
	
	public static Scene GetScene()
	{
		if(scene == null)
			scene = new Scene();
		return scene;
	}
	
	public static void ResetScene()
	{
		scene = null;
	}
}
