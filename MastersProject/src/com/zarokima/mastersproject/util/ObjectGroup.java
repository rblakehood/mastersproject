package com.zarokima.mastersproject.util;

public enum ObjectGroup
{
	Walls ("Walls"),
	PlayerSpawn ("Player Spawn"),
	Goal ("Goal"),
	Spikes ("Spikes"),
	Guns ("Gun"),
	Blocks ("Blocks");
	
	private final String name;
	
	private ObjectGroup(String s)
	{
		name = s;
	}
	
	public String getName()
	{
		return name;
	}
	
	public static ObjectGroup fromString(String s)
	{
		if(s == null)
			return null;
		
		for(ObjectGroup g : ObjectGroup.values())
		{
			if(s.equalsIgnoreCase(g.getName()))
				return g;
		}
		return null;
	}
	
	public String toString()
	{
		return name;
	}
}
