package com.zarokima.mastersproject.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import com.zarokima.mastersproject.MenuActivity;
import android.graphics.PorterDuff;

public class LevelAdapter extends BaseAdapter
{
	private Context context;

	private ArrayList<String> levelNames;
	private ArrayList<String> completeLevels;
	
	public LevelAdapter(Context c)
	{
		context = c;
		levelNames = new ArrayList<String>();
		completeLevels = new ArrayList<String>();
		try
		{
			String[] files = context.getAssets().list("tmx");
			for (String file : files)
			{
				if (file.endsWith(".tmx"))
				{
					//only want the base name
					levelNames.add(file.substring(0, file.indexOf(".tmx")));
				}
			}
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BufferedReader buf;
		try
		{
			buf = new BufferedReader(new InputStreamReader(context.openFileInput(MenuActivity.SAVE_FILE)));
			String s;
			while((s = buf.readLine()) != null)
			{
				completeLevels.add(s);
			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public int getCount()
	{
		return levelNames.size();
	}

	@Override
	public Object getItem(int index)
	{
		return levelNames.get(index);
	}

	@Override
	public long getItemId(int arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		Button button;
		if (convertView == null)
		{
			button = new Button(context);
			button.setText(levelNames.get(position));
			button.setTextSize(12);
			button.setTextColor(Color.rgb(0, 0, 0));
			button.setPadding(8, 8, 8, 8);
			if(completeLevels.contains(levelNames.get(position)))
			{
				button.getBackground().setColorFilter(0xFF00FF00, PorterDuff.Mode.DST_OVER);
			}
			button.setOnClickListener((MenuActivity) context);
		}
		else
		{
			button = (Button) convertView;
		}

		return button;
	}

}
