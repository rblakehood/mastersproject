package com.zarokima.mastersproject.util;

import org.andengine.extension.physics.box2d.PhysicsWorld;
import android.hardware.SensorManager;
import com.badlogic.gdx.math.Vector2;

//singleton so I don't have to pass it around everywhere from the game activity
public class PhysicsWorldHelper
{
	private static PhysicsWorld physicsWorld;
	
	public static PhysicsWorld GetPhysicsWorld()
	{
		if(physicsWorld == null)
		{
			physicsWorld = new PhysicsWorld(new Vector2(0, SensorManager.GRAVITY_EARTH), false);
			physicsWorld.setContinuousPhysics(false);
			physicsWorld.setContactListener(new MastersContactListener());
		}
		
		return physicsWorld;
	}
	
	public static void ResetPhysicsWorld()
	{
		physicsWorld = null;
	}
}
