package com.zarokima.mastersproject.util;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class MastersContactListener implements ContactListener
{

	@Override
	public void beginContact(Contact contact)
	{
		UserData a = (UserData) contact.getFixtureA().getBody().getUserData();
		UserData b = (UserData) contact.getFixtureB().getBody().getUserData();

			a.container.beginCollideWith(contact.getFixtureB().getBody());

			b.container.beginCollideWith(contact.getFixtureA().getBody());
	}

	@Override
	public void endContact(Contact contact)
	{
		UserData a = (UserData) contact.getFixtureA().getBody().getUserData();
		UserData b = (UserData) contact.getFixtureB().getBody().getUserData();

			a.container.endCollideWith(contact.getFixtureB().getBody());

			b.container.endCollideWith(contact.getFixtureA().getBody());
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse)
	{
		// TODO Auto-generated method stub

	}

}
