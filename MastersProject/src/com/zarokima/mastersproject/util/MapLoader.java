package com.zarokima.mastersproject.util;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.entity.scene.Scene;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXObject;
import org.andengine.extension.tmx.TMXObjectGroup;
import org.andengine.extension.tmx.TMXObjectProperty;
import org.andengine.extension.tmx.TMXProperties;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.extension.tmx.util.exception.TMXLoadException;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.PointF;
import android.util.Log;
import com.zarokima.mastersproject.game.Block;
import com.zarokima.mastersproject.game.Goal;
import com.zarokima.mastersproject.game.Gun;
import com.zarokima.mastersproject.game.Spikes;
import com.zarokima.mastersproject.game.Wall;

public class MapLoader
{
	public static TMXTiledMap tileMap;
	public static PointF playerSpawn;

	public static void LoadMap(Context context, BoundCamera camera, AssetManager assetManager,
			TextureManager textureManager, TextureOptions textureOptions,
			VertexBufferObjectManager vertexBufferObjectManager, String path)
	{
		playerSpawn = new PointF(32, 32);
		Scene scene = SceneHelper.GetScene();
		try
		{
			TMXLoader tmxLoader = new TMXLoader(assetManager, textureManager, textureOptions, vertexBufferObjectManager);
			tileMap = tmxLoader.loadFromAsset(path);
		}
		catch (TMXLoadException tmxle)
		{
			Debug.e(tmxle);
		}

		// Add the non-object layers to the scene
		for (int i = 0; i < tileMap.getTMXLayers().size(); i++)
		{
			TMXLayer layer = tileMap.getTMXLayers().get(i);
			if (layer.getName().contains("Tile Layer"))
				scene.attachChild(layer);
		}

		createMapObjects(context, scene, vertexBufferObjectManager, textureManager);

		// Make the camera not exceed the bounds of the map
		TMXLayer tmxLayer = tileMap.getTMXLayers().get(0);
		camera.setBounds(0, 0, tmxLayer.getWidth(), tmxLayer.getHeight());
		camera.setBoundsEnabled(true);
	}
	
	private static void createMapObjects(Context context, Scene scene, VertexBufferObjectManager vbom, TextureManager textureManager)
	{
		for (TMXObjectGroup group : tileMap.getTMXObjectGroups())
		{
			ObjectGroup og = ObjectGroup.fromString(group.getName());
			if(og == null)
				continue;
			switch(og)
			{
				case Walls:
					makeWalls(group, vbom);
					break;
				case PlayerSpawn:
					makePlayerSpawn(group);
					break;
				case Goal:
					makeGoal(group, context, textureManager, vbom);
					break;
				case Spikes:
					makeSpikes(group, vbom);
					break;
				case Guns:
					makeGuns(group, context, textureManager, vbom);
					break;
				case Blocks:
					makeBlocks(group, context, textureManager, vbom);
					break;
			}
		}
	}

	private static void makeWalls(TMXObjectGroup group, VertexBufferObjectManager vbom)
	{
		for (final TMXObject object : group.getTMXObjects())
		{
			new Wall(object.getX(), object.getY(), object.getWidth(), object.getHeight(), vbom);
		}
	}
	
	private static void makePlayerSpawn(TMXObjectGroup group)
	{
		TMXObject object = group.getTMXObjects().get(0); //should only ever be 1
		playerSpawn = new PointF(object.getX(), object.getY());
	}
	
	private static void makeGoal(TMXObjectGroup group, Context context, TextureManager textureManager, VertexBufferObjectManager vbom)
	{
		TMXObject object = group.getTMXObjects().get(0); //should only ever be 1
		new Goal(context, new PointF(object.getX(), object.getY()), textureManager, vbom);
	}
	
	private static void makeSpikes(TMXObjectGroup group, VertexBufferObjectManager vbom)
	{
		for (final TMXObject object : group.getTMXObjects())
		{
			new Spikes(object.getX(), object.getY(), object.getWidth(), object.getHeight(), vbom);
		}
	}
	
	private static void makeGuns(TMXObjectGroup group, Context context, TextureManager textureManager, VertexBufferObjectManager vbom)
	{
		for (final TMXObject object : group.getTMXObjects())
		{
			TMXProperties<TMXObjectProperty> properties = object.getTMXObjectProperties();
			int direction = 8;
			int delay = 1;
			int speed = 5;
			for( TMXObjectProperty property : properties)
			{
				if(property.getName().equals("Direction"))
				{
					direction = Integer.parseInt(property.getValue());
				}
				else if(property.getName().equals("ShootDelay"))
				{
					delay = Integer.parseInt(property.getValue());
				}
				else if(property.getName().equals("ShootSpeed"))
				{
					speed = Integer.parseInt(property.getValue());
				}
			}
			new Gun(context, new PointF(object.getX(), object.getY()), textureManager, vbom, direction, delay, speed);
		}
	}
	
	private static void makeBlocks(TMXObjectGroup group, Context context, TextureManager textureManager, VertexBufferObjectManager vbom)
	{
		for(final TMXObject object : group.getTMXObjects())
		{
			new Block(context, new PointF(object.getX(), object.getY()), object.getWidth(), object.getHeight(), textureManager, vbom);
		}
	}
}
