package com.zarokima.mastersproject.util;

import com.badlogic.gdx.math.Vector2;

//angle math is hard
//circles suck
public class GravityHelper
{
	private static final String TAG = "GravityHelper";
	
	public static final Vector2 DEFAULT_GRAVITY = new Vector2(0, 10); 
	
	public static Vector2 currentGravity = DEFAULT_GRAVITY;
	
	//rotates the input vector to match the current gravity
	public static Vector2 CalculateDirectionalVelocity(Vector2 velocity)
	{
		velocity.rotate(GetGravityAngle());
		velocity.y = 0 - velocity.y; //I don't know why this is needed, but it makes it work
		return velocity;
	}
	
	public static float GetGravityAngle()
	{
		//no idea why I need to subtract from 360 but it works
		return 360 - currentGravity.angle() - DEFAULT_GRAVITY.angle();
	}
	
	//un-rotates the input vector to so it goes with the default gravity
	public static Vector2 CalculateUnRotatedVelocity(Vector2 velocity)
	{
		velocity.y = 0 - velocity.y;
		velocity.rotate(360 - GetGravityAngle());
		return velocity;
	}
	
	public static void SetGravityAngle(float angle)
	{
		currentGravity = DEFAULT_GRAVITY.rotate(angle - DEFAULT_GRAVITY.angle());
		PhysicsWorldHelper.GetPhysicsWorld().setGravity(currentGravity);
	}
}
