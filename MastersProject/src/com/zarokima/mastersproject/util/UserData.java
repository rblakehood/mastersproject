package com.zarokima.mastersproject.util;

import java.lang.reflect.Field;

//Everything with a body should set its user data to an instance of this
public class UserData
{
	public boolean isPlayer;
	public boolean isCharacter;
	public boolean isEnemy;
	public boolean isGround;
	public boolean isGoal;
	public boolean killOnContact;
	
	public ICollidable container = null;
	
	@Override
	//print all fields that are true
	//i.e. a list of descriptors about the containing object
	public String toString()
	{
		Field[] fields = this.getClass().getFields();
		
		String s = "";
		
		for(Field field : fields)
		{
			try
			{
				if(field.getBoolean(this))
				{
					s += field.getName() + " ";
				}
			}
			catch (Exception e)
			{
				// do nothing
			}

		}
		
		return s;
	}
}
