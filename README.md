MastersProject
==============

This is my Master's Project for college. It's a 2D platformer for Android, 
where gravity is always real-world down. As such, rotating your phone 
causes gravity in the game to rotate accordingly. Since this is a fairly
unexplored feature of platformers, I won't know where to take it from there
until I get to the point where I can start making levels to see how the
dynamic gravity affects gameplay and find out what's fun.
